export const environment = {
  production: true,
  apiUrl: 'https://reqres.in/api',
  rfidSensorUrl: 'http://localhost:3000'
};
