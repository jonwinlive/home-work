import { Routes } from '@angular/router';
import { ErrorComponent } from './error/error.component';

export const ROUTES: Routes = [
    { path: '', redirectTo: 'app', pathMatch: 'full'},
    { path: 'app', 
    loadChildren: ( ) => import('./tasks/tasks.module').then( m => m.TasksModule ) },
    { path: 'error', component: ErrorComponent },
    { path: '**', component: ErrorComponent }
];