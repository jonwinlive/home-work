import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { Observable, of as observableOf } from 'rxjs';
//Model
import { Pagination } from '../models';

interface ReourcesResult<T> {
    resources: T[],
    pagination: Pagination;
}

interface IndexParams {
    page?: number;
    pageSize?: number;
    sort?: any;
    filters?: any;
}

@Injectable()
export abstract class CrudService<T> {
    constructor( protected http: HttpClient ){}
    abstract createResource( params: any ): T;
    abstract resourceName({plural}?: {plural?: boolean}): string;
    resourcePath({parentId}: {parentId?: number} = {}): string {
        return this.resourceName({plural: true});
    }
    all({page, pageSize, sort, filters}: IndexParams = {}, pathOptions?: {parentId?: number}): Observable<ReourcesResult<T>> {

        let params = new HttpParams()
          .set('page', page ? page.toString() : '1');
    
        for (const key in filters) {
          if (filters.hasOwnProperty(key)) {
            params = params.set(key, filters[key]);
          }
        }
        return this.http.get<any>( `${environment.apiUrl}/${this.resourcePath(pathOptions)}` , { params:  params } )
          .pipe(
            map(data => {
                console.log(' DATOS ', data);
              return {
                pagination: new Pagination(data.meta),
                resources: data[this.resourceName({plural: true})].map(c => this.createResource(c))
              } as ReourcesResult<T>;
            })
          );
      }
}