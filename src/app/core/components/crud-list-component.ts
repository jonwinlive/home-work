import { ViewChild } from '@angular/core';
import { Pagination } from '../models';
import { CrudService } from '../services/crud.service';

export class CrudListComponent<T> {
    @ViewChild('tableTmpl', { static: false }) table: any;
    parentId: number;
    loading = false;
    rows: T[] = [];
    columns = [];
    page: Pagination = new Pagination();
    sort = { direction: 'desc', prop: 'id' };
    constructor( private resourceSrv: CrudService<T>){
    }
    protected loadResources(page?: number, filters?: any) {
        this.loading = true;
        const params = {page: page, pageSize: this.page.pageSize, sort: this.sort, filters: filters};
    
        this.resourceSrv.all(params, this.pathOptions).subscribe(data => {
          this.rows = data.resources;
          this.page = data.pagination;
          this.loading = false;
        }, error => {
          this.loading = false;
        });
    }
    private get pathOptions(): {parentId: number} {
        return this.parentId ? {parentId: this.parentId} : undefined;
    }
}