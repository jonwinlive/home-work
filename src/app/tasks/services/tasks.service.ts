import { Injectable } from "@angular/core";
import { CrudService } from 'src/app/core/services/crud.service';
import { Activity } from '../models/activity';

@Injectable({
    providedIn: 'root'
})
export class TasksService extends CrudService<Activity> {
    createResource(params: any): Activity {
        return params  as Activity;
    }
    resourceName({ plural }: { plural?: boolean; }): string {
        return plural ? 'users': 'user';
    }
}