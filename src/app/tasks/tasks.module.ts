import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgFormAdminTasksComponent } from './ng-form-admin-tasks/ng-form-admin-tasks.component';
import { RouterModule } from '@angular/router';
import { NgListTasksComponent } from './ng-list-tasks/ng-list-tasks.component';
import { HttpClientModule } from '@angular/common/http';

export const routes = [
  { path: '', component: NgListTasksComponent, pathMatch: 'full'}
];

@NgModule({
  declarations: [ NgFormAdminTasksComponent, NgListTasksComponent ],
  imports: [
    CommonModule,
   
    RouterModule.forChild( routes)
  ]
})
export class TasksModule { 
  static routes = routes;
}
