export class Activity {
    id: number;
    name: string;
    description: string;
    duration: string;
    constructor( json ){
        this.id = json.id;
        this.name = json.name;
        this.duration = json.duration; 
    }
    get idActivity(): number {
        return this.id ? this.id : -1;
    }
}