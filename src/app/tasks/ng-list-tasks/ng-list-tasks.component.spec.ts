import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgListTasksComponent } from './ng-list-tasks.component';

describe('NgListTasksComponent', () => {
  let component: NgListTasksComponent;
  let fixture: ComponentFixture<NgListTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgListTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgListTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
