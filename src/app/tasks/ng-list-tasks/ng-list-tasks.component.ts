import { Component, OnInit } from '@angular/core';
import { CrudListComponent } from 'src/app/core/components';
import { Activity } from '../models/activity';
import { TasksService } from '../services/tasks.service';

@Component({
  selector: 'app-ng-list-tasks',
  templateUrl: './ng-list-tasks.component.html',
  styleUrls: ['./ng-list-tasks.component.scss']
})
export class NgListTasksComponent extends CrudListComponent<Activity>  implements OnInit {

  constructor(  
    activitySrv: TasksService
  ) { 
    super( activitySrv );
  }

  ngOnInit(): void {
    this.loadResources();
  }

}
