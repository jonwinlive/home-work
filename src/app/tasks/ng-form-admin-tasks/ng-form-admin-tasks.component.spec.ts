import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgFormAdminTasksComponent } from './ng-form-admin-tasks.component';

describe('NgFormAdminTasksComponent', () => {
  let component: NgFormAdminTasksComponent;
  let fixture: ComponentFixture<NgFormAdminTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgFormAdminTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgFormAdminTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
